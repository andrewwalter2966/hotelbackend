<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Booking;
use App\Model;
use Faker\Generator as Faker;

$factory->define(booking::class, function (Faker $faker) {
    return [
        //
        'name'=> $faker->name,
        'numberofnights'=> $faker->NumberBetween($min = 1, $max = 21),
        'numberofpeople'=> $faker->NumberBetween($min = 1, $max = 10),
        'dateofbooking'=> $faker->date($max = 'now'),
        'checkindate'=> $faker->dateTimeBetween($startDate = 'now', $endDate = '+1 year'),
        'checkoutdate'=> $faker->dateTimeBetween($startDate = 'checkindate', $endDate = '+21 days')
    ];
});
