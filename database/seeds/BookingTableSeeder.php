<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Generator as Faker;
// use \factories\BookingFactory;
use App\Booking;

class BookingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(booking::class,6)->create();
        // DB::table('bookings')-> insert([
        //     'name' => 'John',
        //     'numberofnights' => '3',
        //     'numberofpeople' => '2',
        //     'dateofbooking' => '2020-4-23',
        //     'checkindate' => '2020-8-12',
        //     'checkoutdate' => '2020-8-15'
        // ]);
    }
}
