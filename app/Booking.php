<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = [
        'numberofnights',
        'numberofpeople',
        'name',
        'dateofbooking'
    ];
    protected $dates = [
        'dateofbooking'
    ];
}
